#if defined _franugknife_included
  #endinput
#endif
#define _franugknife_included

native int Franug_GetKnife(int client);

public void __pl_knives_SetNTVOptional() 
{
	MarkNativeAsOptional("Franug_GetKnife");
}